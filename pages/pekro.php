
<div id="pekro-banner" class="top-banner">
	<div class="container-fluid text-center">
		<h1 class="color-black">Pekro.cz</h1>
	</div>
</div>
<div class="reference-page">
	<div class="container">
        <div class="reference-info">
        	<p>
        	Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. 
        	Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus. Purus Cum nec id tempor porttitor et Ut mus mauris Curabitur. Sed Nulla justo Quisque auctor Lorem tortor a faucibus Suspendisse.
        	</p>
        </div>
    </div>
    <div class="container-fluid">
    	<div class="reference-row triangle reference-right lazy" data-src="/images/layout/references/pekro/pekro_bg_right.png" id="pekro">
    		<h2 class="text-center d-lg-none color-black">Pekro</h2>
    		<div class="row mx-0 h-100">
    			<div class="col-12 col-xl-2"></div>
      			<div class="col-12 col-lg-6 col-xl-5 p-0 reference-img">
      				<img data-src="/images/layout/references/pekro/pekro_devices.png" class="img-fluid lazy" alt="Pekro - reference laptop"/>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-5 reference-content">
      				<h2 class="d-none d-lg-block color-black">Pekro</h2>
      				<div class="reference-text">
      					<p>
      					Lorem ipsum dolor sit amet consectetuer semper at malesuada vel libero. Nam tempor tellus laoreet rutrum Aliquam enim non leo quis Aenean. 
      					Sed ut Quisque habitasse a wisi sagittis mollis massa et feugiat. Montes Vivamus Nullam in vel Aenean felis nibh mauris Cum semper. 
      					Sed ac Curabitur nascetur Vestibulum lorem In sit id ligula et. Aliquam nascetur et et sollicitudin sem feugiat Donec.
      					</p>
      				</div>
      				<div class="reference-sum">
            			<div class="reference-data">
            				<div><span class="font-weight-bold">515</span> <span class="sum-time">hodin</span></div>
            				<div class="sum-type">Grafické práce</div>
            			</div>
            			<div class="reference-data">
            				<div><span class="font-weight-bold">1000</span> <span class="sum-time">hodin</span></div>
            				<div class="sum-type">Programování</div>
            			</div>
            			<div class="reference-data">
            				<div><span class="font-weight-bold">+50</span> <span class="sum-time">hodin</span></div>
            				<div class="sum-type">Tech. podpora</div>
            			</div>   				
            		</div>
            		<div class="reference-tag">
            			<a class="btn btn-tag" href="/">Responzivní design</a>
            			<a class="btn btn-tag" href="/">HTML5</a>
            			<a class="btn btn-tag" href="/">LESS</a>
            			<a class="btn btn-tag" href="/">jQuery</a>
            			<a class="btn btn-tag" href="/">PHP</a>
            			<a class="btn btn-tag" href="/">C#</a>
            			<a class="btn btn-tag" href="/">SQL</a>
            			<a class="btn btn-tag" href="/">Redakční systém</a>
            			<a class="btn btn-tag" href="/">Dopravci</a>
            			<a class="btn btn-tag" href="/">Platební brány</a>
            			<a class="btn btn-tag" href="/">Jednokrokový košík</a>
            			<a class="btn btn-tag" href="/">Srovnávače</a>
            			<a class="btn btn-tag" href="/">Napojení na ekonomický systém</a>
            			<a class="btn btn-tag" href="/">SSL certifikát</a>
            			<a class="btn btn-tag" href="/">Technická podpora</a>
            		</div>
    			</div>
    		</div>
    	</div>
    	<div class="reference-row reference-left slash-green">
    		<h2 class="text-center d-lg-none">Internetový obchod</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-6 px-0 text-right effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-1.jpg" class="img-fluid lazy" alt="Pekro - reference 1"/>
          				</div>
          				<div class="col-12 col-sm-6 px-0 text-left effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-2.jpg" class="img-fluid lazy" alt="Pekro - reference 2"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Internetový obchod</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					Lorem ipsum dolor sit amet consectetuer justo aliquet nec nunc tincidunt. Laoreet et mauris ut malesuada lacus Morbi tincidunt quis ac Curabitur. 
      					Elit leo turpis gravida ligula Pellentesque purus a eros tortor et. Penatibus pede pretium at vitae ridiculus cursus pede pretium massa at. 
      					Vel et Morbi tellus ut fringilla vel fames quis nibh tincidunt. Hendrerit ut justo porttitor velit lobortis quis tincidunt nascetur pulvinar.
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-row reference-right slash-green">
    		<h2 class="text-center d-lg-none">3 barevné varianty</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-6 px-0 text-right effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-3.jpg" class="img-fluid lazy" alt="Pekro - reference 3"/>
          				</div>
          				<div class="col-12 col-sm-6 px-0 text-left effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-4.jpg" class="img-fluid lazy" alt="Pekro - reference 4"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">3 barevné varianty</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					<strong class="color-blue">Prodej techniky</strong>, <strong class="color-yellow">Servis</strong>, <strong class="color-green">Spotřební materiál</strong>, <strong class="color-red">Gamezone</strong> dolor sit amet consectetuer justo aliquet nec nunc tincidunt. Laoreet et mauris ut malesuada lacus Morbi tincidunt quis ac Curabitur. 
      					Elit leo turpis gravida ligula Pellentesque purus a eros tortor et. Penatibus pede pretium at vitae ridiculus cursus pede pretium massa at. 
      					Vel et Morbi tellus ut fringilla vel fames quis nibh tincidunt. Hendrerit ut justo porttitor velit lobortis quis tincidunt nascetur pulvinar.
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-row reference-left slash-green">
    		<h2 class="text-center d-lg-none">Merketingové prvky</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-6 px-0 text-right effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-5.jpg" class="img-fluid lazy" alt="Pekro - reference 5"/>
          				</div>
          				<div class="col-12 col-sm-6 px-0 text-left effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-6.jpg" class="img-fluid lazy" alt="Pekro - reference 6"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Merketingové prvky</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					Neodeslaný košík, Odeslání souvisejícího zboží po 14ti dnech, hlídací pes, končící slevový kupon, newsletter, sdílený košík, články
      					</p>
      				</div>
    			</div>
    		</div>
    		<div class="col-12 col-xl-2"></div>
    	</div>
    	<div class="reference-row reference-right bg-yellow">
    		<h2 class="text-center d-lg-none">Servis kopírek, tiskáren a faxů všech typů a značek</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-6 px-0 text-right effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-7.jpg" class="img-fluid lazy" alt="Pekro - reference 7"/>
          				</div>
          				<div class="col-12 col-sm-6 px-0 text-left effect-img-hover">
          					<img data-src="/images/layout/references/pekro/pekro-8.jpg" class="img-fluid lazy" alt="Pekro - reference 8"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Servis kopírek, tiskáren a faxů všech typů a značek</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					Online zjištění stavu servisu, informováné emailem, servis různých druhů zařízení, dotaz technikovi
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-row reference-left slash-green hexagon">
    		<h2 class="text-center d-lg-none">Napojení na účetní systém Helios</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-12 px-0 text-center">
          					<img data-src="/images/layout/references/pohoda.png" class="img-fluid lazy" alt="Pohoda - logo"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Napojení na účetní systém Helios</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					Produkty, skladovost, objednávky, zákazníci, dodavetelé
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-row reference-right slash-green">
    		<h2 class="text-center d-lg-none">Napojení na dodavatele</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-12 px-0 text-center">
            				<img data-src="/images/layout/references/logo/lama.png" class="img-fluid m-2 lazy" alt="Lama - logo"/>
            				<img data-src="/images/layout/references/logo/100mega.png" class="img-fluid m-2 lazy" alt="100Mega - logo"/>
            				<img data-src="/images/layout/references/logo/swis.png" class="img-fluid m-2 lazy" alt="SWIS - logo"/>
            				<img data-src="/images/layout/references/logo/atcomputers.png" class="img-fluid m-2 lazy" alt="ATComputers - logo"/>
            				<img data-src="/images/layout/references/logo/arles.png" class="img-fluid m-2 lazy" alt="Arles - logo"/>
            				<img data-src="/images/layout/references/logo/damedis.png" class="img-fluid m-2 lazy" alt="Damedis - logo"/>
            				<img data-src="/images/layout/references/logo/ed.png" class="img-fluid m-2 lazy" alt="ed - logo"/>
            				<img data-src="/images/layout/references/logo/techdata.png" class="img-fluid m-2 lazy" alt="TechData - logo"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Napojení na dodavatele</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					Lama, SWS, ATC, atd... Doplnit všechnny dodavatele + kecy okolo co všechno se aktualizuje.
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-panel">
    		<div class="row mx-0 align-items-center">
    			<div class="col-12 col-md-6 col-lg-3 col-xl-4 panel-box">
    				<a class="panel-link" href="https://www.pekro.cz/">Zobrazit e-shop <strong>Pekro</strong> online <i class="fas fa-external-link-alt"></i></a>
    			</div>
    			<div class="col-12 col-md-12 col-lg-6 col-xl-4 panel-box">
    				<div class="text-uppercase mb-3">Máte zájem o podobný projekt?</div>
    				<a class="btn btn-off btn-off-light" href="/">Chci nabídku</a>
    			</div>
    			<div class="col-12 col-md-6 col-lg-3 col-xl-4 panel-box">
    				<a class="panel-link" href="/">Zobrazit více <strong>podobných projektů</strong> <i class="fas fa-angle-double-right"></i></a>
    			</div>
    		</div>
    	</div>
    	<div class="row mx-0">
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item web-presentation custom-development show">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/bewooden/bewooden-desktop.jpg" class="img-fluid lazy" alt="BeWooden - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">BeWooden.cz</h4>
            		<p class="grid-info">
            		Tvoříme produkty s příběhem, inspirujeme se přírodou a obnovujeme krásu řemesel. Dřevěné motýlky, kožené peněženky, zvířecí brože, náramky, kšandy, klobouky a další doplňky z prvotřídních materiálů. 
            		Říkáme, že vše vyrábíme ručně. Pravdou však je, že vše děláme srdcem.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
				<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid lazy" alt="FanaticShopEU - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid lazy" alt="Pekro - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid lazy" alt="FanaticShopEU - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid lazy" alt="Pekro - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>  
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item web-presentation custom-development">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/bewooden/bewooden-desktop.jpg" class="img-fluid lazy" alt="BeWooden - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">BeWooden.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
			</div>            
			<div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid lazy" alt="FanaticShopEU - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            <a class="grid-display" href="/">
            	<img data-src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid lazy" alt="Pekro - miniatura"/>
        	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
        </div>
    </div>
</div>