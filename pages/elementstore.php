

<div id="elementstore-banner" class="top-banner">
	<div class="container-fluid text-center">
		<h1 class="color-black">Elementstore.cz</h1>
	</div>
</div>
<div class="reference-page">
	<div class="container">
        <div class="reference-info">
        	<p>
        	Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. 
        	Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus. Purus Cum nec id tempor porttitor et Ut mus mauris Curabitur. Sed Nulla justo Quisque auctor Lorem tortor a faucibus Suspendisse.
        	</p>
        </div>
    </div>
    <div class="container-fluid">
    	<div class="reference-row triangle reference-right lazy" data-src="/images/layout/references/elementstore/elementstore_bg_right.png" id="elementstore">
    		<h2 class="text-center d-lg-none color-black">Elementstore</h2>
    		<div class="row mx-0 h-100">
    			<div class="col-12 col-xl-2"></div>
      			<div class="col-12 col-lg-6 col-xl-5 p-0 reference-img">
      				<img data-src="/images/layout/references/elementstore/elementstore_devices.png" class="img-fluid lazy" alt="Elementstore - reference laptop"/>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-5 reference-content">
      				<h2 class="d-none d-lg-block color-black">Elementstore</h2>
      				<div class="reference-text">
      					<p>
      					Lorem ipsum dolor sit amet consectetuer semper at malesuada vel libero. Nam tempor tellus laoreet rutrum Aliquam enim non leo quis Aenean. 
      					Sed ut Quisque habitasse a wisi sagittis mollis massa et feugiat. Montes Vivamus Nullam in vel Aenean felis nibh mauris Cum semper. 
      					Sed ac Curabitur nascetur Vestibulum lorem In sit id ligula et. Aliquam nascetur et et sollicitudin sem feugiat Donec.
      					</p>
      				</div>
      				<div class="reference-sum">
            			<div class="reference-data">
            				<div><span class="font-weight-bold">515</span> <span class="sum-time">hodin</span></div>
            				<div class="sum-type">Grafické práce</div>
            			</div>
            			<div class="reference-data">
            				<div><span class="font-weight-bold">1000</span> <span class="sum-time">hodin</span></div>
            				<div class="sum-type">Programování</div>
            			</div>
            			<div class="reference-data">
            				<div><span class="font-weight-bold">+50</span> <span class="sum-time">hodin</span></div>
            				<div class="sum-type">Tech. podpora</div>
            			</div>   				
            		</div>
            		<div class="reference-tag">
            			<a class="btn btn-tag" href="/">Responzivní design</a>
            			<a class="btn btn-tag" href="/">HTML5</a>
            			<a class="btn btn-tag" href="/">LESS</a>
            			<a class="btn btn-tag" href="/">jQuery</a>
            			<a class="btn btn-tag" href="/">PHP</a>
            			<a class="btn btn-tag" href="/">C#</a>
            			<a class="btn btn-tag" href="/">SQL</a>
            			<a class="btn btn-tag" href="/">Redakční systém</a>
            			<a class="btn btn-tag" href="/">Dopravci</a>
            			<a class="btn btn-tag" href="/">Platební brány</a>
            			<a class="btn btn-tag" href="/">Jednokrokový košík</a>
            			<a class="btn btn-tag" href="/">Srovnávače</a>
            			<a class="btn btn-tag" href="/">Napojení na ekonomický systém</a>
            			<a class="btn btn-tag" href="/">SSL certifikát</a>
            			<a class="btn btn-tag" href="/">Technická podpora</a>
            		</div>
    			</div>
    		</div>
    	</div>
    	<div class="reference-row reference-left slash-green">
    		<h2 class="text-center d-lg-none">Internetový obchod</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-6 px-0 text-right effect-img-hover">
          					<img data-src="/images/layout/references/elementstore/elementstore-1.jpg" class="img-fluid lazy" alt="Elementstore - reference 1"/>
          				</div>
          				<div class="col-12 col-sm-6 px-0 text-left effect-img-hover">
          					<img data-src="/images/layout/references/elementstore/elementstore-2.jpg" class="img-fluid lazy" alt="Elementstore - reference 2"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Internetový obchod</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					Lorem ipsum dolor sit amet consectetuer justo aliquet nec nunc tincidunt. Laoreet et mauris ut malesuada lacus Morbi tincidunt quis ac Curabitur. 
      					Elit leo turpis gravida ligula Pellentesque purus a eros tortor et. Penatibus pede pretium at vitae ridiculus cursus pede pretium massa at. 
      					Vel et Morbi tellus ut fringilla vel fames quis nibh tincidunt. Hendrerit ut justo porttitor velit lobortis quis tincidunt nascetur pulvinar.
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-row reference-right slash-green">
    		<h2 class="text-center d-lg-none">Propracovaný detail produktu</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-6 px-0 text-right effect-img-hover">
          					<img data-src="/images/layout/references/elementstore/elementstore-3.jpg" class="img-fluid lazy" alt="Elementstore - reference 3"/>
          				</div>
          				<div class="col-12 col-sm-6 px-0 text-left effect-img-hover">
          					<img data-src="/images/layout/references/elementstore/elementstore-4.jpg" class="img-fluid lazy" alt="Elementstore - reference 4"/>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Propracovaný detail produktu</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					<strong>Skladovost jednotlivých variant, hodnocení heureka, přidávání komentářů, související zboží, odeslat produkt známému.</strong> Lorem Ipsum dolor sit amet consectetuer justo aliquet nec nunc tincidunt. 
      					Laoreet et mauris ut malesuada lacus Morbi tincidunt quis ac Curabitur. Elit leo turpis gravida ligula Pellentesque purus a eros tortor et. 
      					Penatibus pede pretium at vitae ridiculus cursus pede pretium massa at. Vel et Morbi tellus ut fringilla vel fames quis nibh tincidunt. 
      					Hendrerit ut justo porttitor velit lobortis quis tincidunt nascetur pulvinar.
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-row reference-left slash-green">
    		<h2 class="text-center d-lg-none">Rozsáhlý backend s mnoha funkcemi</h2>
    		<div class="row mx-0 h-100">
      			<div class="col-12 col-lg-6 col-xl-6 reference-img">
      				<div class="row mx-0 w-100">
          				<div class="col-12 col-sm-12 px-0 text-center carousel3d-box">
          					<div class="carousel3d">
                                <div class="carousel3d-content">
                                    <div class="carousel3d-item lazy" data-src="/images/example/slide1.jpg"></div>
                                    <div class="carousel3d-item lazy" data-src="/images/example/slide2.jpg"></div>
                                    <div class="carousel3d-item lazy" data-src="/images/example/slide3.jpg"></div>
                                </div>
                            </div>
          				</div>
      				</div>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-4 reference-content">
      				<h2 class="d-none d-lg-block">Rozsáhlý backend s mnoha funkcemi</h2>
      				<div class="reference-text">
      					<p class="text-justify">
      					<strong>Štítky, fakturace, emailový klient, finanční dashboard, exporty, kasa,  správa zákazníků, zboží. </strong> 
      					Lorem ipsum dolor sit amet consectetuer justo aliquet nec nunc tincidunt. Laoreet et mauris ut malesuada lacus Morbi tincidunt quis ac Curabitur. 
      					Elit leo turpis gravida ligula Pellentesque purus a eros tortor et. Penatibus pede pretium at vitae ridiculus cursus pede pretium massa at. 
      					Vel et Morbi tellus ut fringilla vel fames quis nibh tincidunt. Hendrerit ut justo porttitor velit lobortis quis tincidunt nascetur pulvinar.
      					</p>
      				</div>
    			</div>
    			<div class="col-12 col-xl-2"></div>
    		</div>
    	</div>
    	<div class="reference-panel">
    		<div class="row mx-0 align-items-center">
    			<div class="col-12 col-md-6 col-lg-3 col-xl-4 panel-box">
    				<a class="panel-link" href="https://www.elementstore.cz/">Zobrazit e-shop <strong>Elementstore</strong> online <i class="fas fa-external-link-alt"></i></a>
    			</div>
    			<div class="col-12 col-md-12 col-lg-6 col-xl-4 panel-box">
    				<div class="text-uppercase mb-3">Máte zájem o podobný projekt?</div>
    				<a class="btn btn-off btn-off-light" href="/">Chci nabídku</a>
    			</div>
    			<div class="col-12 col-md-6 col-lg-3 col-xl-4 panel-box">
    				<a class="panel-link" href="/">Zobrazit více <strong>podobných projektů</strong> <i class="fas fa-angle-double-right"></i></a>
    			</div>
    		</div>
    	</div>
    	<div class="row mx-0">
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item web-presentation custom-development show">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/bewooden/bewooden-desktop.jpg" class="img-fluid lazy" alt="BeWooden - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">BeWooden.cz</h4>
            		<p class="grid-info">
            		Tvoříme produkty s příběhem, inspirujeme se přírodou a obnovujeme krásu řemesel. Dřevěné motýlky, kožené peněženky, zvířecí brože, náramky, kšandy, klobouky a další doplňky z prvotřídních materiálů. 
            		Říkáme, že vše vyrábíme ručně. Pravdou však je, že vše děláme srdcem.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
				<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid lazy" alt="FanaticShopEU - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid lazy" alt="Pekro - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid lazy" alt="FanaticShopEU - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid lazy" alt="Pekro - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>  
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item web-presentation custom-development">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/bewooden/bewooden-desktop.jpg" class="img-fluid lazy" alt="BeWooden - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">BeWooden.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
			</div>            
			<div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
            	<a class="grid-display" href="/">
            		<img data-src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid lazy" alt="FanaticShopEU - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            <a class="grid-display" href="/">
            	<img data-src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid lazy" alt="Pekro - miniatura"/>
        	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
        </div>
    </div>
</div>