

<div id="references-banner" class="top-banner">
	<div class="container-fluid text-center">
		<h1>Reference</h1>
	</div>
</div>
<div id="references-grid">
    <div class="container-fluid">
        <div class="filter-button-group">
        	<div class="container">
                <button class="btn btn-filter active" data-filter="*">Vše <span>(40)</span></button>
                <button class="btn btn-filter" data-filter=".e-shop">E-shopy <span>(21)</span></button>
                <button class="btn btn-filter" data-filter=".web-presentation">Webové prezentace <span>(14)</span></button>
                <button class="btn btn-filter" data-filter=".custom-development">Vývoj na míru <span>(3)</span></button>
                <button class="btn btn-filter" data-filter=".eis">EIS <span>(2)</span></button>
            </div>
        </div>
        <div class="row mx-0 grid">
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item web-presentation custom-development show">
            	<a class="grid-display" href="/">
            		<img src="/images/layout/references/bewooden/bewooden-desktop.jpg" class="img-fluid" alt="BeWooden - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">BeWooden.cz</h4>
            		<p class="grid-info">
            		Tvoříme produkty s příběhem, inspirujeme se přírodou a obnovujeme krásu řemesel. Dřevěné motýlky, kožené peněženky, zvířecí brože, náramky, kšandy, klobouky a další doplňky z prvotřídních materiálů. 
            		Říkáme, že vše vyrábíme ručně. Pravdou však je, že vše děláme srdcem.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
				<a class="grid-display" href="/">
            		<img src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid" alt="FanaticShopEU - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            	<a class="grid-display" href="/">
            		<img src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid" alt="Pekro - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
            	<a class="grid-display" href="/">
            		<img src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid" alt="FanaticShopEU - miniatura"/>
            	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            	<a class="grid-display" href="/">
            		<img src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid" alt="Pekro - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>  
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item web-presentation custom-development">
            	<a class="grid-display" href="/">
            		<img src="/images/layout/references/bewooden/bewooden-desktop.jpg" class="img-fluid" alt="BeWooden - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">BeWooden.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
			</div>            
			<div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop web-presentation">
            	<a class="grid-display" href="/">
            		<img src="/images/layout/references/fanaticshop/fanaticshopeu-laptop.jpg" class="img-fluid" alt="FanaticShopEU - miniatura"/>
        		</a>
            	<div class="grid-box">
            		<h4 class="grid-name">FanaticShop.eu</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 p-0 grid-item e-shop eis">
            <a class="grid-display" href="/">
            	<img src="/images/layout/references/pekro/pekro-laptop.jpg" class="img-fluid" alt="Pekro - miniatura"/>
        	</a>
            	<div class="grid-box">
            		<h4 class="grid-name">Pekro.cz</h4>
            		<p class="grid-info">
            		Lorem ipsum dolor sit amet consectetuer vel ipsum cursus id tincidunt.
            		</p>
            		<div class="grid-data">
                		<div class="reference-data">Grafické práce</div>
                		<div class="reference-data">Programování</div>
                		<div class="reference-data">Tech. podpora</div>
            		</div>
            		<a class="btn btn-more" href="/">Zobrazit více</a>
            	</div>
            </div>
        </div>
        <div class="text-center">
    		<a class="btn btn-off" href="/">Chci nabídku</a>
    	</div>
    </div>
</div>