

<div id="contact-banner" class="top-banner">
	<div class="container-fluid text-center">
		<h1>Kontakt</h1>
	</div>
</div>
<div id="contact">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-6 offset-lg-1 col-lg-5 px-0">
				<div class="big-contact justify">
					<a href="tel:+420 776 714 651" class="contact-icon"><img class="img-fluid lazy" data-src="images/svg/smartphone.svg"></a>
					<a href="tel:+420 776 714 651" class="contact-link">+420 776 714 651</a>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-5 px-0">
				<div class="big-contact justify">
					<a href="mailto:info@pp-soft.cz" class="contact-icon"><img class="img-fluid lazy" data-src="images/svg/envelope.svg"></a>
					<a href="mailto:info@pp-soft.cz" class="contact-link">info@pp-soft.cz</a>
				</div>
			</div>
		</div>
		<div class="info-contact">
			<div class="row mx-0">
				<div class="col-12 col-md-5">
					<h2>Fakturační údaje</h2>
					<address><strong>PP Soft s.r.o.</strong> <br>Prostřední 132 <br>686 01 Uherské Hradiště <br><br>IČ: 04419791 <br>DIČ: CZ04419791 <br><br>Datová schránka: rz29b8p</address>
				</div>
				<div class="col-12 col-md-7 p-0">
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="team">
	<div class="container">
		<h1>Náš team</h1>
		<div class="row mx-0">
			<div class="col-12 team-member">
				<img class="img-fluid member-avatar lazy" alt="Bc. Petr Procházka" data-src="images/layout/contact/prochazka_petr.png">
				<h6 class="member-name">Bc. Petr Procházka</h6>
				<div class="member-info">CEO</div>
				<a class="member-contact" href="tel:+420 776 714 651">+420 776 714 651</a>
				<a class="member-contact" href="mailto:info@pp-soft.cz">info@pp-soft.cz</a>
			</div>
			<div class="col-12 col-sm-6 col-md-4 team-member">
				<img class="img-fluid member-avatar lazy" alt="Jan Hajduch" data-src="images/layout/contact/hajduch_jan.png">
				<h6 class="member-name">Jan Hajduch</h6>
				<div class="member-info">Projektový manager, programátor</div>
				<a class="member-contact" href="tel:+420 722 010 794">+420 722 010 794</a>
				<a class="member-contact" href="mailto:hajduch@pp-soft.cz">hajduch@pp-soft.cz</a>
			</div>
			<div class="col-12 col-sm-6 col-md-4 team-member">
				<img class="img-fluid member-avatar lazy" alt="Bc. Radim Kolísek" data-src="images/layout/contact/kolisek_radim.png">
				<h6 class="member-name">Bc. Radim Kolísek</h6>
				<div class="member-info">Projektový manager, grafik</div>
				<a class="member-contact" href="tel:+420 607 433 823">+420 607 433 823</a>
				<a class="member-contact" href="mailto:kolisek@pp-soft.cz">kolisek@pp-soft.cz</a>
			</div>
			<div class="col-12 col-sm-4 col-md-4 team-member">
				<img class="img-fluid member-avatar lazy" alt="Ondřej Hájek" data-src="images/layout/contact/hajek_ondrej.png">
				<h6 class="member-name">Ondřej Hájek</h6>
				<div class="member-info">Programátor</div>
				<a class="member-contact" href="tel:+420 607 331 516">+420 607 331 516</a>
				<a class="member-contact" href="mailto:hajek@pp-soft.cz">hajek@pp-soft.cz</a>
			</div>
			<div class="col-12 col-sm-4 col-md-4 team-member">
				<img class="img-fluid member-avatar lazy" alt="Jakub Mareček" data-src="images/layout/contact/marecek_jakub.png">
				<h6 class="member-name">Jakub Mareček</h6>
				<div class="member-info">Programátor</div>
				<a class="member-contact" href="mailto:marecek@pp-soft.cz">marecek@pp-soft.cz</a>
			</div>
			<div class="col-12 col-md-4 team-member new">
				<img class="img-fluid member-avatar lazy" alt="Avatar" data-src="images/layout/contact/avatar.png">
				<h6 class="member-name">Čekáme právě na tebe!</h6>
				<div class="member-info">Kontaktuj nás na</div>
				<a class="member-contact" href="mailto:info@pp-soft.cz">info@pp-soft.cz</a>
			</div>
			<div class="col-12 col-sm-4 col-md-4 team-member">
				<img class="img-fluid member-avatar lazy" alt="David Uherek" data-src="images/layout/contact/uherek_david.png">
				<h6 class="member-name">David Uherek</h6>
				<div class="member-info">Programátor</div>
				<a class="member-contact" href="mailto:uherek@pp-soft.cz">uherek@pp-soft.cz</a>
			</div>
		</div>
	</div>
</div>
<div id="story">
	<div class="layout">
    	<div class="container">
    		<h1>Příběh</h1>
    		<div id="timeline">
    			<div class="year">
    				<div class="year_storys">
    					<div class="time-line">
    						<div class="time-line-rel">
    							<div class="time-line-item time-line-item-left year-mark fadeInDown wow">
									<div class="time-line-date">2009</div>
									<div class="time-line-dot"></div>
    							</div>
    						</div>
    					</div>
    					<div class="story right_story slideInRight wow" data-wow-duration="2s" style="top: 20%;">
    						<div class="story_outer">
    							<h6 class="story_title">Založení společnosti SmartSoft</h6>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="year">
    				<div class="year_storys">
    					<div class="time-line">
    						<div class="time-line-rel">
    							<div class="time-line-item time-line-item-right year-mark fadeInDown wow">
									<div class="time-line-date">2010</div>
									<div class="time-line-dot"></div>
    							</div>
    							<div class="time-line-item time-line-item-left fadeInDown wow" style="top: 75%;">
									<div class="time-line-date">Srpen</div>
									<div class="time-line-dot"></div>
    							</div>
    						</div>
    					</div>
    					<div class="story left_story slideInLeft wow" data-wow-duration="2s" style="top: 20%;">
    						<div class="story_outer">
    							<h6 class="story_title">Spuštěno prvních 50 projektů</h6>
    							<p>Mezi největší spuštěné projekty patří: pekro.cz, amilo.cz, pmu.no, hq-elektro.cz atd....</p>
    						</div>
    					</div>
    					<div class="story right_story slideInRight wow" data-wow-duration="2s" style="top: 40%;">
    						<div class="story_outer">
    							<h6 class="story_title">Neque penatibus hendrerit</h6>
    							<p>Mezi největší spuštěné projekty patří: pekro.cz, amilo.cz, pmu.no, hq-elektro.cz atd....</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="year">
    				<div class="year_storys">
    					<div class="time-line">
    						<div class="time-line-rel">
    							<div class="time-line-item time-line-item-right year-mark fadeInDown wow">
									<div class="time-line-date">2011</div>
									<div class="time-line-dot"></div>
    							</div>
    						</div>
    					</div>
    					<div class="story left_story slideInLeft wow" data-wow-duration="2s" style="top: 10%;">
    						<div class="story_outer">
    							<h6 class="story_title">Neque penatibus hendrerit</h6>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    						</div>
    					</div>
    					<div class="story right_story slideInRight wow" data-wow-duration="2s" style="top: 20%;">
    						<div class="story_outer">
    							<h6 class="story_title">Neque penatibus hendrerit</h6>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    						</div>
    					</div>
    					<div class="story left_story slideInLeft wow" data-wow-duration="2s" style="top: 40%;">
    						<div class="story_outer">
    							<h6 class="story_title">Neque penatibus hendrerit</h6>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    							<p>Lorem ipsum dolor sit amet consectetuer sem dolor ut pharetra pretium. Eu pellentesque mattis dictum justo id interdum Pellentesque ipsum id sollicitudin. Neque penatibus hendrerit In et Vivamus semper interdum Curabitur dui metus.</p>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
	</div>
</div>