
<div id="home">
   <div class="container-fluid">
   		<div class="row">
   			<div class="col-12 col-sm-6 d-flex align-items-center min-height-js min-height-js-xs">
   				<div class="justify-content-end col-12 col-sm-11 offset-sm-1 px-0">
					<h1>Předáním zakázky<br />naše služby <span class="color-cyan">nekončí</span></h1>
					<h2>Lorem ipsum dolor sit amet consectetuer Nam id augue justo ligula. </h2>
					<p>Lorem ipsum dolor sit amet consectetuer Nam id augue justo ligula. Id Integer aliquet eleifend Cum hendrerit ipsum auctor dictum semper risus. Vel mauris Vivamus sagittis dignissim consequat sapien congue orci molestie Curabitur. Wisi id sit et Suspendisse tincidunt urna in dui Nulla pretium. Lacus neque aliquet at urna wisi id </p>
				</div>
			</div>
   			<div class="col-12 col-sm-6 d-flex align-items-center min-height-js">
   				<div class="justify-content-start col-11 ">
					<img src="/images/svg/collage.svg" class="home-collage" />
				</div>
			</div>
   		</div>
		<div class="scroll-down animated bounce hvr-bob scroll-to" data-href="stats">
			<img src="images/svg/mouse.svg" alt="scroll-down-mouse" class="scroll-down-mouse">
			<img src="images/svg/down-arrow-scroll.svg" alt="scroll-down-arrow" class="scroll-down-arrow">					
		</div>
   </div>
</div>
<div id="stats" class="lazy">
	<div class="container-fluid">
        <div class="row">
        	<div class="col-6 col-lg-3 text-center px-0 stat-col">           			
				<div class="d-inline-block align-middle text-right px-1">
					<img data-src="/images/svg/calendar.svg" class="d-inline-block stat-icon lazy"/>
				</div>
				<div class="d-inline-block align-middle text-left px-1"> 
					<div class="colored-fonts d-inline-block pt-1"><span class="counter">8</span> <span>l</span><span>e</span><span>t</span></div>
				</div>
    			<div class="clearfix"></div>
    			<span class="subtext">na trhu</span>
        	</div>
        	<div class="col-6 col-lg-3 text-center px-0 stat-col">
				<div class="d-inline-block align-middle text-right px-1">
					<img data-src="/images/svg/diamond.svg" class="d-inline-block stat-icon lazy"/>
				</div>
				<div class="d-inline-block align-middle text-left px-1"> 
					<div class="colored-fonts d-inline-block pt-1"><span class="counter">6</span><span class="counter">0</span><span>+</span></div>
				</div>
    			<div class="clearfix"></div>
    			<span class="subtext">hotových projektů</span>
        	</div>
        	<div class="col-6 col-lg-3 text-center px-0 stat-col">	
				<div class="d-inline-block align-middle text-right px-1">
					<img data-src="/images/svg/menu.svg" class="d-inline-block stat-icon lazy"/>
				</div>
				<div class="d-inline-block align-middle text-left px-1"> 
					<div class="colored-fonts d-inline-block pt-1"><span class="counter">8</span><span class="counter">4</span><span class="counter">9</span> <span class="counter">9</span><span class="counter">0</span><span class="counter">0</span><span>+</span></div>
				</div>
    			<div class="clearfix"></div>
    			<span class="subtext">řádků za rok</span>
        	</div>
        	<div class="col-6 col-lg-3 text-center px-0 stat-col">
				<div class="d-inline-block align-middle text-right px-1">
					<img data-src="/images/svg/clock.svg" class="d-inline-block stat-icon lazy"/>
				</div>
				<div class="d-inline-block align-middle text-left px-1"> 
					<div class="colored-fonts d-inline-block pt-1"><span class="counter">1</span><span class="counter">3</span><span class="counter">5</span><span class="counter">0</span></div>
				</div>
    			<div class="clearfix"></div>
    			<span class="subtext">hodin konzultací</span>
        	</div>
        </div>
	</div>
</div>
<div id="why" class="lazy" data-src="/images/layout/homepage/office.png">
    <div class="container">
    	<div class="row ">
     		<div class="col-sm-2 col-xl-4 p-0 d-flex left-background"></div> 	
     		<div class="col-12 col-sm-10 col-xl-8 px-sm-0 why-content">	
         		<h1>PROČ ZVOLIT <br class="why-title"/> PP-SOFT?</h1>
         		<div class="why-boxes">
             		<div class="why-row">
    					<div class="row mx-0 why-benefit">
    						<div class="why-icon"><img data-src="/images/layout/homepage/icon_1.png" class="why-icon-img lazy"/></div>
    						<div class="why-info">
    							<h2>Lorem Ipsum</h2>
    							<p>Lorem ipsum dolor sit amet consectetuer a tellus et laoreet nunc. Quisque in non pretium Vestibulum parturient Donec est ante Vivamus et. </p>
    						</div>
    					</div>
    					<div class="why-row">
    						<div class="row mx-0 why-benefit">
    							<div class="why-icon"><img data-src="/images/layout/homepage/icon_2.png" class="why-icon-img lazy"/></div>
    							<div class="why-info">
    								<h2>Lorem Ipsum</h2>
    								<p>Lorem ipsum dolor sit amet consectetuer a tellus et laoreet nunc. Quisque in non pretium Vestibulum parturient Donec est ante Vivamus et. </p>
    							</div>
    						</div>
							<div class="why-row">
    							<div class="row mx-0 why-benefit">
        							<div class="why-icon"><img data-src="/images/layout/homepage/icon_3.png" class="why-icon-img lazy"/></div>
        							<div class="why-info">
        								<h2>Lorem Ipsum</h2>
        								<p>Lorem ipsum dolor sit amet consectetuer a tellus et laoreet nunc. Quisque in non pretium Vestibulum parturient Donec est ante Vivamus et. </p>
        							</div>
    							</div>
    							<div class="why-row"> 
    								<a href="/" class="btn btn-off">Chci nabídku</a>
    							</div>
    						</div>
    					</div>
    				</div>
         		</div>
         		<img data-src="/images/layout/homepage/bottom_shadow.png" class="bottom-shadow lazy"/>
     		</div> 
    	</div>	
    </div>
</div>
<div id="references" class="lazy">
    <div class="container-fluid">
    	<div class="reference-head">
    		<h1>Naše reference</h1>
    	</div>
    	<div class="reference-row triangle lazy" data-src="/images/layout/references/bewooden/bewooden_bg_left.png" id="bewooden">
    		<h2 class="text-center d-lg-none color-green">Bewooden</h2>
    		<div class="row mx-0 h-100">
    			<div class="col-12 col-xl-2"></div>
      			<div class="col-12 col-lg-6 col-xl-5 p-0 reference-img">
      				<img data-src="/images/layout/references/bewooden/bewooden_devices.png" class="img-fluid lazy" alt="Bewooden - laptop"/>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-5 reference-content">
      				<h2 class="d-none d-lg-block color-green">Bewooden</h2>
      				<div class="reference-text">
      					<p>
      					Lorem ipsum dolor sit amet consectetuer semper at malesuada vel libero. Nam tempor tellus laoreet rutrum Aliquam enim non leo quis Aenean. 
      					Sed ut Quisque habitasse a wisi sagittis mollis massa et feugiat. Montes Vivamus Nullam in vel Aenean felis nibh mauris Cum semper. 
      					Sed ac Curabitur nascetur Vestibulum lorem In sit id ligula et. Aliquam nascetur et et sollicitudin sem feugiat Donec.
      					</p>
      				</div>
      				<div class="reference-link">
      					<div class="row mx-0 align-items-center">
    						<div class="col-4 col-sm-3 text-center">
    							<img data-src="/images/layout/references/bewooden.png" class="img-fluid lazy" alt="Bewooden - logo"/>
    						</div>
    						<div class="col-8 col-sm-5 col-xl-4 p-0">
    							<a class="color-green" href="https://www.bewooden.cz/">bewooden.cz <i class="fas fa-external-link-alt"></i></a>
    							<label>sk, de, at, com, pl, eu, fi, se</label>
    						</div>
          					<div class="col-12 col-sm-4 offset-xl-1">
          						<img data-src="/images/layout/references/helios.png" class="img-fluid lazy" alt="Helios - logo"/>
          						<label>Napojení na informační systém</label>
          					</div>
          				</div>
      				</div>
    			</div>
    		</div>
    		<div class="reference-sum justify">
    			<div class="reference-data">
    				<div><span class="font-weight-bold">515</span> <span class="sum-time">hodin</span></div>
    				<div class="sum-type">Grafické práce</div>
    			</div>
    			<div class="reference-data">
    				<div><span class="font-weight-bold">1000</span> <span class="sum-time">hodin</span></div>
    				<div class="sum-type">Programování</div>
    			</div>
    			<div class="reference-data">
    				<div><span class="font-weight-bold">+50</span> <span class="sum-time">hodin</span></div>
    				<div class="sum-type">Tech. podpora</div>
    			</div>   				
    		</div>
    	</div>
    	<div class="reference-row triangle lazy" data-src="/images/layout/references/austin/austin_bg_right.png" id="austin">
    		<h2 class="text-center d-lg-none color-red">Austin Detonator</h2>
    		<div class="row mx-0 h-100">
    			<div class="col-12 col-xl-2"></div>
    			<div class="col-12 col-lg-6 col-xl-5 p-0 reference-img">
      				<img data-src="/images/layout/references/austin/austin_devices.png" class="img-fluid lazy" alt="Austin Detonator - laptop"/>
      			</div>
      			<div class="col-12 col-lg-6 col-xl-5 reference-content">
      				<h2 class="d-none d-lg-block color-red">Austin Detonator</h2>
      				<div class="reference-text">
      					<p>
      					Lorem ipsum dolor sit amet consectetuer semper at malesuada vel libero. Nam tempor tellus laoreet rutrum Aliquam enim non leo quis Aenean. 
      					Sed ut Quisque habitasse a wisi sagittis mollis massa et feugiat. Montes Vivamus Nullam in vel Aenean felis nibh mauris Cum semper. 
      					Sed ac Curabitur nascetur Vestibulum lorem In sit id ligula et. Aliquam nascetur et et sollicitudin sem feugiat Donec.
      					</p>
      				</div>
      				<div class="reference-link">
      					<div class="row mx-0 align-items-center">
    						<div class="col-4 offset-sm-2 col-sm-3 text-center">
    							<img data-src="/images/layout/references/austin.svg" class="img-fluid lazy" alt="Austin Detonator - logo"/>
    						</div>
    						<div class="col-8 col-sm-5 p-0">
    							<a class="color-red" href="https://www.austin.cz/">austin.cz <i class="fas fa-external-link-alt"></i></a>
    						</div>
          				</div>
      				</div>
    			</div>
    		</div>
    		<div class="reference-sum justify">
    			<div class="reference-data">
    				<div><span class="font-weight-bold">515</span> <span class="sum-time">hodin</span></div>
    				<div class="sum-type">Grafické práce</div>
    			</div>
    			<div class="reference-data">
    				<div><span class="font-weight-bold">1000</span> <span class="sum-time">hodin</span></div>
    				<div class="sum-type">Programování</div>
    			</div>
    			<div class="reference-data">
    				<div><span class="font-weight-bold">+50</span> <span class="sum-time">hodin</span></div>
    				<div class="sum-type">Tech. podpora</div>
    			</div>   				
    		</div>
    	</div>
    	<div class="reference-row view" id="energy">
    		<h2 class="text-center color-yellow">Energy IS</h2>
    		<div class="d-flex justify-content-center">
    			<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-5">
          			<div class="reference-text">
        				<p>Lorem ipsum dolor sit amet consectetuer semper at malesuada vel libero. Nam tempor tellus laoreet rutrum Aliquam enim non leo quis Aenean. Sed ut Quisque</p>
        			</div>
    			</div>
    		</div>
    		<div class="row mx-0 h-100">
    			<div class="col-12 col-md-4 p-md-0 reference-img effect-img-hover">
      				<img data-src="/images/layout/references/energy/energy-1.jpg" class="img-fluid lazy" alt="Energy IS - Klientská část EIS"/>
      				<a class="btn reference-image-text" href="/">
      					<span>Klientská část EIS</span>
      				</a>
      			</div>
      			<div class="col-12 col-md-4 p-md-0 reference-img effect-img-hover">
      				<img data-src="/images/layout/references/energy/energy-2.jpg" class="img-fluid lazy" alt="Energy IS - Administrátorská část EIS"/>
      				<a class="btn reference-image-text" href="/">
      					<span>Administrátorská část EIS</span>
      				</a>
      			</div>
      			<div class="col-12 col-md-4 p-md-0 reference-img effect-img-hover">
      				<img data-src="/images/layout/references/energy/energy-3.jpg" class="img-fluid lazy" alt="Energy IS - Prezentace společnosti"/>
      				<a class="btn reference-image-text" href="/">
      					<span>Prezentace společnosti</span>
      				</a>
      			</div>
    		</div>
    		<div class="row mx-0">
    			<div class="col-12 col-lg-7 offset-xl-1 col-xl-6 pr-0">
          			<div class="reference-sum justify">
        				<div class="reference-data">
        					<div><span class="font-weight-bold">515</span> <span class="sum-time">hodin</span></div>
        					<div class="sum-type">Grafické práce</div>
        				</div>
        				<div class="reference-data">
        					<div><span class="font-weight-bold">1000</span> <span class="sum-time">hodin</span></div>
        					<div class="sum-type">Programování</div>
        				</div>
        				<div class="reference-data">
        					<div><span class="font-weight-bold">+50</span> <span class="sum-time">hodin</span></div>
        					<div class="sum-type">Tech. podpora</div>
        				</div>   				
        			</div>
    			</div>
    			<div class="col-12 col-lg-5 col-xl-4">
    				<div class="reference-sum justify">
        				<img data-src="/images/layout/references/free-for-you.png" class="img-fluid lazy" alt="Free for you - logo"/>
        				<img data-src="/images/layout/references/ray-energy.png" class="img-fluid lazy" alt="Ray Energy - logo"/>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="text-center">
    		<a class="btn btn-off" href="/">Chci nabídku</a>
    	</div>
    </div>
</div>
<div id="offer" class="lazy" data-src="/images/layout/homepage/offer_bg.png">
    <div class="container-fluid">
    	<h1>Co nabízíme</h1>
    	<div class="container">
    		<div class="row">
    			<div class="col-12 col-lg-4 offer-layout">
    				<div class="offer-box">
    					<img class="img-fluid offer-icon lazy" data-src="/images/svg/browser.svg" alt="browser-icon"/>
    					<h3>Profi e-shop</h3>
    					<div class="offer-info text-center">
    						<p class="italic">Profesionální e-shop, který vydělává. Nenabízíme open source, ale náš systém, který můžeme upravit dle potřeb zákazníka.</p>
    					</div>
    					<div class="offer-list">
      						<ul class="list-unstyled">
      							<li>Individuální přístup k zakázce</li>
      							<li>Tvorba unikátní grafiky</li>
      							<li>Řešení pro B2B i B2C</li>
      							<li>Multiměnovost a multijazyčnost</li>
      							<li>Více e-shopů z jedné administrace</li>
      							<li>Marketingové nástroje</li>
      							<li>Napojení na ERP a EIS</li>
      							<li>Poimplementační podpora</li>
      						</ul>
      					</div>
    				</div>
    			</div>
    			<div class="col-12 col-lg-4 offer-layout">
    				<div class="offer-box">
    					<img class="img-fluid offer-icon lazy" data-src="/images/svg/time-management.svg" alt="time-management-icon"/>
    					<h3>Vývoj na míru</h3>
    					<div class="offer-info text-center">
    						<p>Webové informační systémy pro malé i velké firmy. Dodáváme Vám přesně to, co potřebujete</p>
    					</div>
    					<div class="offer-list">
      						<ul class="list-unstyled">
      							<li>Řešení ušité na míru</li>
      							<li>Kompletní pokrytí agendy</li>
      							<li>Systém poroste s Vaší firmou</li>
      							<li>Žádné paušální poplatky</li>
      							<li>Neomezený počet uživatelů</li>
      							<li>Přehledná administrace</li>
      							<li>Možnosti nastavení práv</li>
      							<li>Přístup odkudkoliv</li>
      						</ul>
    					</div>
    				</div>
    			</div>
    			<div class="col-12 col-lg-4 offer-layout">
    				<div class="offer-box">
    					<img class="img-fluid offer-icon lazy" data-src="/images/svg/idea.svg" alt="idea-icon"/>
    					<h3>Energy IS</h3>
    					<div class="offer-info text-center">
    						<p>Řešení pro fakturaci elektřiny a plynu. Vedení zákazníků a komunikace distributory.</p>
    					</div>
    					<div class="offer-list">
      						<ul class="list-unstyled">
      							<li>Prodej elektřiny a plynu</li>
      							<li>Zákaznický portál</li>
      							<li>Partnerský portál</li>
      							<li>Kompletní procesy změny dodavatele</li>
      							<li>Automatická fakturace</li>
      							<li>Platby, SIPO, banka</li>
      							<li>Smlouvy a dokumenty</li>
      							<li>Automatizované procesy - Workflow</li>
      						</ul>
      					</div>
    				</div>
    			</div>
    		</div>
    		<div class="quality-panel">
    			<div class="row mx-0">
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center"><img class="img-fluid lazy" data-src="/images/layout/homepage/design.png" alt="design-icon"></div>
    						<div class="col-9 pr-0">Webdesign</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center">
    							<img class="img-fluid lazy" data-src="/images/layout/homepage/responsive.png" alt="responsive-icon">
    						</div>
    						<div class="col-9 pr-0">Responzivní design</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center">
    							<img class="img-fluid lazy" data-src="/images/layout/homepage/actual.png" alt="actual-icon">
    						</div>
    						<div class="col-9 pr-0">Poslední standardy</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center">
    							<img class="img-fluid lazy" data-src="/images/layout/homepage/screen.png" alt="screen-icon">
    						</div>
    						<div class="col-9 pr-0">Front end</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center">
    							<img class="img-fluid lazy" data-src="/images/layout/homepage/setting.png" alt="setting-icon">
    						</div>
    						<div class="col-9 pr-0">Backend</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center">
    							<img class="img-fluid lazy" data-src="/images/layout/homepage/globe.png" alt="globe-icon">
    						</div>
    						<div class="col-9 pr-0">Různé jazyky</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
        					<div class="col-3 p-0 text-center">
        						<img class="img-fluid lazy" data-src="/images/layout/homepage/phone.png" alt="phone-icon">
        					</div>
        					<div class="col-9 pr-0">Mobilní aplikace</div>
    					</a>
    				</div>
    				<div class="col-12 col-sm-6 col-lg-3 p-0 quality-layout">
    					<a class="quality-box gradient-hover" href="/">
    						<div class="col-3 p-0 text-center">
    							<img class="img-fluid lazy" data-src="/images/layout/homepage/code.png" alt="code-icon">
							</div>
    						<div class="col-9 pr-0">Čistý kód</div>
    					</a>
    				</div>
    			</div>
    			<div class="gaus-blur"></div>
    		</div>
    	</div>
    </div>
</div>
<div id="review" class="lazy" data-src="/images/layout/homepage/review_bg.png">
    <div class="container-fluid">
    	<h1>Řekli O nás</h1>   		
		<div class="row mx-0 review-row">
			<div class="col-2">
				<div class="arrow prv-arrw">
					<i class="fas fa-chevron-left"></i>
				</div>
			</div>
			<div class="col-8">
    			<div class="review-slider">
        			<?php for($x=0; $x<13; $x++) { ?>
                		<div class="review-box">
                			<img data-src="/images/example/review_avatar.png" class="img-fluid rounded-circle review-img lazy" alt="Tomáš Jedno">
                			<div class="review-head">Lorem Ipsum</div>
                			<div class="review-name">Tomáš Jedno, <span class="bold">pekro.cz</span></div>
                			<div class="review-text">
                				<p class="italic">
                				Sem a consequat laoreet netus id Suspendisse tempus pulvinar mauris elit. Dui risus velit ut ante laoreet semper wisi congue nulla ac. 
                				Dui eget Nulla ipsum tellus mus hendrerit et pede consequat Aliquam. Pellentesque Vivamus a eget eu ut In libero a interdum scelerisque. 
                				Cursus semper et Morbi at in In eu ullamcorper Curabitur faucibus. Pretium Quisque quis ut justo volutpat tempor odio metus fames metus. Consequat.
                				</p>
                			</div>
                			<div class="review-quote lazy" data-src="/images/layout/homepage/quote.png"></div>
                		</div>
                	<?php } ?>
    			</div>
			</div>
			<div class="col-2">
    			<div class="arrow nxt-arrw">
					<i class="fas fa-chevron-right"></i>
				</div>
			</div>  			
		</div>
    </div>
</div>
<div id="world" class="lazy" data-src="/images/layout/homepage/world_bg.png">
    <div class="container-fluid">
    	<h1>S námi do světa</h1>   		
		<div class="row mx-0 world-row">
			<div class="col-12 offset-lg-1 col-lg-2">
				<img data-src="/images/svg/czech-republic.svg" class="img-fluid world-flag lazy" alt="Česko"/>
				<img data-src="/images/svg/slovakia.svg" class="img-fluid world-flag lazy" alt="Slovensko"/>
				<img data-src="/images/svg/germany.svg" class="img-fluid world-flag lazy" alt="Německo"/>
				<img data-src="/images/svg/belgium.svg" class="img-fluid world-flag lazy" alt="Belgie"/>
				<img data-src="/images/svg/poland.svg" class="img-fluid world-flag lazy" alt="Polsko"/>
				<img data-src="/images/svg/austria.svg" class="img-fluid world-flag lazy" alt="Rakousko"/>
				<img data-src="/images/svg/denmark.svg" class="img-fluid world-flag lazy" alt="Dánsko"/>
				<img data-src="/images/svg/sweden.svg" class="img-fluid world-flag lazy" alt="Švédsko"/>
				<img data-src="/images/svg/united-kingdom.svg" class="img-fluid world-flag lazy" alt="Velká Británie"/>
			</div>
			<div class="col-12 col-lg-6">
				<div class="world-text">
					<p>
					Sem a consequat laoreet netus id Suspendisse tempus pulvinar mauris elit. Dui risus velit ut ante laoreet semper wisi congue nulla ac. 
					Dui eget Nulla ipsum tellus mus hendrerit et pede consequat Aliquam. Pellentesque Vivamus a eget eu ut In libero a interdum scelerisque. 
					Cursus semper et Morbi at in In eu ullamcorper Curabitur faucibus. Pretium Quisque quis ut justo volutpat tempor odio metus fames metus. Consequat.
					</p>
				</div>
			</div>
		</div>
    </div>
</div>