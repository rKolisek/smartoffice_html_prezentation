$(document).ready(function() {

	$(document).on("click", ".cd-nav-trigger", function() {
		if($(this).hasClass("close-nav")) {
			$(this).removeClass("close-nav");
			$("#mobile-menu").animate({left: "-100%"}, 1000);
		} else {
			$(this).addClass("close-nav");
			$("#mobile-menu").show().animate({left: "0px"}, 1000, function() {
		    	// Animation complete.
			});
		}
	});

	$(".counter").counterUp({
		delay: 10,
		time: 500
	});
	
	function slideMenu() {
		$("#main-menu .dropdown").on('show.bs.dropdown', function() {
			$(this).find(".dropdown-menu").slideDown(300);
		});
		$("#main-menu .dropdown").on('hide.bs.dropdown', function() {
			$(this).find(".dropdown-menu").slideUp(300);
		});
	}
	
	function displayMenu() {
		if (viewportSize.getWidth() < 992) {
			$("#mobile-menu").show();
			if ($("#menu-response").find("#main-menu").length == 0) {
				$("#main-menu").clone().appendTo($("#menu-response"));
			}
		} else {
			$("#mobile-menu").hide();
			$("#menu-response").find("#main-menu").remove();
		}
		slideMenu();
	}
	
	function setMenuWidth() {
		if (viewportSize.getWidth() >= 576 && viewportSize.getWidth() < 992) {
			$("#mobile-menu").css("width", $(".cd-nav-trigger").offset().left);
		} else {
			$("#mobile-menu").css("width", "250px");
		}
	}

	function setHomeHeight() {
		var window_height = viewportSize.getHeight(),
			window_width = viewportSize.getWidth(),
			home = $("#home"),
			top_height = $("#top").outerHeight();
			height = window_height - top_height;
		
		$("#menu-response").css("height", height);
		$(".container-fluid", home).css("min-height", (height + parseInt(60)));
		
		if (window_width <= 576){                 
			height = (height / 2); 
			$(".min-height-js-xs", home).css("min-height", (height));
			$(".min-height-js", home).css("min-height", "auto");
		} else {
			$(".min-height-js", home).css("min-height", (height));
		}
	}
	
	function displayScroll() {
		if ($(this).scrollTop() >= viewportSize.getHeight()/2 && $(window).outerWidth() >= 576) {
	        $("#scroll-top").fadeIn(300);
	    } else {
	        $("#scroll-top").fadeOut(300);
	    }
	}
	
	$(window).scroll(function() {
		displayScroll();
	});

	$(window).on("resize", function() {
		setHomeHeight();
		displayScroll();
		setMenuWidth();
		displayMenu();
		setHeightTimeline();
		wow.init();
	});

	setHomeHeight();
	setMenuWidth();
	displayMenu();
	setHeightTimeline();
	

	$(".lazy").Lazy({
		effect: "fadeIn",
		effectTime: 1000,
        threshold: viewportSize.getHeight()/2,
        defaultImage: "data:image/gif;base64,R0lGODlhIAAgAPUAAP///zLB1vr9/c/w9Oz4+vL6+9ny9pff6a3l7vf8/Or4+vz9/afk7J3h6+f3+cXt8rLn7+/5+73q8eL1+GPP33fV43/Y5ZXe6bjp8PH6+23S4Yja5zvD1zLB1tfy9tLx9d/0+FXL3H3X5ErI2m/T4b/r8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgkFjgcR3HJJE4SxEGnMygKmkwJxRKdVocFBRRLfFAoj6GUOhQoFAVysULRjNdfQFghLxrODEJ4Qm5ifUUXZwQAgwBvEXIGBkUEZxuMXgAJb1dECWMABAcHDEpDEGcTBQMDBQtvcW0RbwuECKMHELEJF5NFCxm1AAt7cH4NuAOdcsURy0QCD7gYfcWgTQUQB6Zkr66HoeDCSwIF5ucFz3IC7O0CC6zx8YuHhW/3CvLyfPX4+OXozKnDssBdu3G/xIHTpGAgOUPrZimAJCfDPYfDin2TQ+xeBnWbHi37SC4YIYkQhdy7FvLdpwWvjA0JyU/ISyIx4xS6sgfkNS4me2rtVKkgw0JCb8YMZdjwqMQ2nIY8BbcUQNVCP7G4MQq1KRivR7tiDEuEFrggACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCQmNBpCcckkEgREA4ViKA6azM8BEZ1Wh6LOBls0HA5fgJQ6HHQ6InKRcWhA1d5hqMMpyIkOZw9Ca18Qbwd/RRhnfoUABRwdI3IESkQFZxB4bAdvV0YJQwkDAx9+bWcECQYGCQ5vFEQCEQoKC0ILHqUDBncCGA5LBiHCAAsFtgqoQwS8Aw64f8m2EXdFCxO8INPKomQCBgPMWAvL0n/ff+jYAu7vAuxy8O/myvfX8/f7/Arq+v0W0HMnr9zAeE0KJlQkJIGCfE0E+PtDq9qfDMogDkGmrIBCbNQUZIDosNq1kUsEZJBW0dY/b0ZsLViQIMFMW+RKKgjFzp4fNokPIdki+Y8JNVxA79jKwHAI0G9JGw5tCqDWTiFRhVhtmhVA16cMJTJ1OnVIMo1cy1KVI5NhEAAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgkChqNQnHJJCYWRMfh4CgamkzFwBOdVocNCgNbJAwGhKGUOjRQKA1y8XOGAtZfgIWiSciJBWcTQnhCD28Qf0UgZwJ3XgAJGhQVcgKORmdXhRBvV0QMY0ILCgoRmIRnCQIODgIEbxtEJSMdHZ8AGaUKBXYLIEpFExZpAG62HRRFArsKfn8FIsgjiUwJu8FkJLYcB9lMCwUKqFgGHSJ5cnZ/uEULl/CX63/x8KTNu+RkzPj9zc/0/Cl4V0/APDIE6x0csrBJwybX9DFhBhCLgAilIvzRVUriKHGlev0JtyuDvmsZUZlcIiCDnYu7KsZ0UmrBggRP7n1DqcDJEzciOgHwcwTyZEUmIKEMFVIqgyIjpZ4tjdTxqRCMPYVMBYDV6tavUZ8yczpkKwBxHsVWtaqo5tMgACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCQuBgNBcck0FgvIQtHRZCYUGSJ0IB2WDo9qUaBQKIXbLsBxOJTExUh5mB4iDo0zXEhWJNBRQgZtA3tPZQsAdQINBwxwAnpCC2VSdQNtVEQSEkOUChGSVwoLCwUFpm0QRAMVFBQTQxllCqh0kkIECF0TG68UG2O0foYJDb8VYVa0alUXrxoQf1WmZnsTFA0EhgCJhrFMC5Hjkd57W0jpDsPDuFUDHfHyHRzstNN78PPxHOLk5dwcpBuoaYk5OAfhXHG3hAy+KgLkgNozqwzDbgWYJQyXsUwGXKNA6fnYMIO3iPeIpBwyqlSCBKUqEQk5E6YRmX2UdAT5kEnHKkQ5hXjkNqTPtKAARl1sIrGoxSFNuSEFMNWoVCxEpiqyRlQY165wEHELAgAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgsKhSLonJJTBIFR0GxwFwmFJlnlAgaTKpFqEIqFJMBhcEABC5GjkPz0KN2tsvHBH4sJKgdd1NHSXILah9tAmdCC0dUcg5qVEQfiIxHEYtXSACKnWoGXAwHBwRDGUcKBXYFi0IJHmQEEKQHEGGpCnp3AiW1DKFWqZNgGKQNA65FCwV8bQQHJcRtds9MC4rZitVgCQbf4AYEubnKTAYU6eoUGuSpu3fo6+ka2NrbgQAE4eCmS9xVAOW7Yq7IgA4Hpi0R8EZBhDshOnTgcOtfM0cAlTigILFDiAFFNjk8k0GZgAxOBozouIHIOyKbFixIkECmIyIHOEiEWbPJTTQ5FxcVOMCgzUVCWwAcyZJvzy45ADYVZNIwTlIAVfNB7XRVDLxEWLQ4E9JsKq+rTdsMyhcEACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuicklMEgVHQVHKVCYUmWeUWFAkqtOtEKqgAsgFcDFyHJLNmbZa6x2Lyd8595h8C48RagJmQgtHaX5XZUYKQ4YKEYSKfVKPaUMZHwMDeQBxh04ABYSFGU4JBpsDBmFHdXMLIKofBEyKCpdgspsOoUsLXaRLCQMgwky+YJ1FC4POg8lVAg7U1Q5drtnHSw4H3t8HDdnZy2Dd4N4Nzc/QeqLW1bnM7rXuV9tEBhQQ5UoCbJDmWKBAQcMDZNhwRVNCYANBChZYEbkVCZOwASEcCDFQ4SEDIq6WTVqQIMECBx06iCACQQPBiSabHDqzRUTKARMhSFCDrc+WNQIcOoRw5+ZIHj8ADqSEQBQAwKKLhIzowEEeGKQ0owIYkPKjHihZoBKi0KFE01b4zg7h4y4IACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuicklMEgVHQVHKVCYUmWeUWFAkqtOtEKqgAsgFcDFyHJLNmbZa6x2Lyd8595h8C48RagJmQgtHaX5XZUUJeQCGChGEin1SkGlubEhDcYdOAAWEhRlOC12HYUd1eqeRokOKCphgrY5MpotqhgWfunqPt4PCg71gpgXIyWSqqq9MBQPR0tHMzM5L0NPSC8PCxVUCyeLX38+/AFfXRA4HA+pjmoFqCAcHDQa3rbxzBRD1BwgcMFIlidMrAxYICHHA4N8DIqpsUWJ3wAEBChQaEBnQoB6RRr0uARjQocMAAA0w4nMz4IOaU0lImkSngYKFc3ZWyTwJAALGK4fnNA3ZOaQCBQ22wPgRQlSIAYwSfkHJMrQkTyEbKFzFydQq15ccOAjUEwQAIfkECQoAAAAsAAAAACAAIAAABv9AgHBILCoUi6JySUwSBUdBUcpUJhSZZ5RYUCSq060QqqACyAVwMXIcks2ZtlrrHYvJ3zn3mHwLjxFqAmZCC0dpfldlRQl5AIYKEYSKfVKQaW5sSENxh04ABYSFGU4LXYdhR3V6p5GiQ4oKmGCtjkymi2qGBZ+6eo+3g8KDvYLDxKrJuXNkys6qr0zNygvHxL/V1sVD29K/AFfRRQUDDt1PmoFqHgPtBLetvMwG7QMes0KxkkIFIQNKDhBgKvCh3gQiqmxt6NDBAAEIEAgUOHCgBBEH9Yg06uWAIQUABihQMACgBEUHTRwoUEOBIcqQI880OIDgm5ABDA8IgUkSwAAyij1/jejAARPPIQwONBCnBAJDCEOOCnFA8cOvEh1CEJEqBMIBEDaLcA3LJIEGDe/0BAEAIfkECQoAAAAsAAAAACAAIAAABv9AgHBILCoUi6JySUwSBUdBUcpUJhSZZ5RYUCSq060QqqACyAVwMXIcks2ZtlrrHYvJ3zn3mHwLjxFqAmZCC0dpfldlRQl5AIYKEYSKfVKQaW5sSENxh04ABYSFGU4LXYdhR3V6p5GiQ4oKmGCtjkymi2qGBZ+6eo+3g8KDvYLDxKrJuXNkys6qr0zNygvHxL/V1sVDDti/BQccA8yrYBAjHR0jc53LRQYU6R0UBnO4RxmiG/IjJUIJFuoVKeCBigBN5QCk43BgFgMKFCYUGDAgFEUQRGIRYbCh2xACEDcAcHDgQDcQFGf9s7VkA0QCI0t2W0DRw68h8ChAEELSJE8xijBvVqCgIU9PjwA+UNzG5AHEB9xkDpk4QMGvARQsEDlKxMCALDeLcA0rqEEDlWCCAAAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgsKhSLonJJTBIFR0FRylQmFJlnlFhQJKrTrRCqoALIBXAxchySzZm2Wusdi8nfOfeYfAuPEWoCZkILR2l+V2VFCXkAhgoRhIp9UpBpbmxIQ3GHTgAFhIUZTgtdh2FHdXqnkaJDigqYYK2OTKaLaoYFn7p6j0wOA8PEAw6/Z4PKUhwdzs8dEL9kqqrN0M7SetTVCsLFw8d6C8vKvUQEv+dVCRAaBnNQtkwPFRQUFXOduUoTG/cUNkyYg+tIBlEMAFYYMAaBuCekxmhaJeSeBgiOHhw4QECAAwcCLhGJRUQCg3RDCmyUVmBYmlOiGqmBsPGlyz9YkAlxsJEhqCubABS9AsPgQAMqLQfM0oTMwEZ4QpLOwvMLxAEEXIBG5aczqtaut4YNXRIEACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuicklMEgVHQVHKVCYUmWeUWFAkqtOtEKqgAsgFcDFyHJLNmbZa6x2Lyd8595h8C48RahAQRQtHaX5XZUUJeQAGHR0jA0SKfVKGCmlubEhCBSGRHSQOQwVmQwsZTgtdh0UQHKIHm2quChGophuiJHO3jkwOFB2UaoYFTnMGegDKRQQG0tMGBM1nAtnaABoU3t8UD81kR+UK3eDe4nrk5grR1NLWegva9s9czfhVAgMNpWqgBGNigMGBAwzmxBGjhACEgwcgzAPTqlwGXQ8gMgAhZIGHWm5WjelUZ8jBBgPMTBgwIMGCRgsygVSkgMiHByD7DWDmx5WuMkZqDLCU4gfAq2sACrAEWFSRLjUfWDopCqDTNQIsJ1LF0yzDAA90UHV5eo0qUjB8mgUBACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuickk0FIiCo6A4ZSoZnRBUSiwoEtYipNOBDKOKKgD9DBNHHU4brc4c3cUBeSOk949geEQUZA5rXABHEW4PD0UOZBSHaQAJiEMJgQATFBQVBkQHZKACUwtHbX0RR0mVFp0UFwRCBSQDSgsZrQteqEUPGrAQmmG9ChFqRAkMsBd4xsRLBBsUoG6nBa14E4IA2kUFDuLjDql4peilAA0H7e4H1udH8/Ps7+3xbmj0qOTj5mEWpEP3DUq3glYWOBgAcEmUaNI+DBjwAY+dS0USGJg4wABEXMYyJNvE8UOGISKVCNClah4xjg60WUKyINOCUwrMzVRARMGENWQ4n/jpNTKTm15J/CTK2e0MoD+UKmHEs4onVDVVmyqdpAbNR4cKTjqNSots07EjzzJh1S0IADsAAAAAAAAAAAA=",
        scrollDirection: "vertical",
        visibleOnly: true,
		lazyLoader: function(element, response) {
    		response(true);
        },
        beforeLoad: function(element) {
        	element.data("src");
        	element.css("opacity", 1);
        },
        afterLoad: function(element) {
        	console.log("Success loading: " +  (element.prop("id").length ? "#" + element.prop("id") : element.prop("alt")) + " \"" + element.data("src") + " \"");
        },
        onError: function(element) {
        	console.log("Error loading: " + (element.prop("id").length ? "#" + element.prop("id") : element.prop("alt")) + " \"" + element.data("src") + "\"");
		}
	});
	
	var grid = $(".grid").isotope({
		itemSelector: ".grid-item",
		layoutMode: "fitRows",
		percentPosition: true
	});
	
	var filters = [];
	
	$(".filter-button-group").on("click", ".btn-filter", function(event) {
		var $target = $(event.currentTarget);
		$target.toggleClass("active");
		var filter = $target.attr("data-filter");
		var $all = $(".btn-filter[data-filter='*']");
		if (filter == "*") {
			resetFilter(filter, $all);
		} else if (filter.length > 1) {
			$(".filter-button-group").find($all).removeClass("active");
			if ($(".filter-button-group").find(".active").length == 0) {
				resetFilter(filter, $all);
			}
			if ($target.hasClass("active")) {
				addFilter(filter);
			} else {
				removeFilter(filter);
			}
			grid.isotope({ filter: filters.join(',') });
		}
	});
	
	function addFilter(filter) {
		if (filters.indexOf(filter) == -1) {
			filters.push(filter);
		}
	}
	
	function removeFilter(filter) {
		var index = filters.indexOf(filter);
		if (index != -1) {
			filters.splice(index, 1);
		}
	}
	
	function resetFilter(filter, $all) {
		$all.parent().find(".active").removeClass("active");
		$all.addClass("active");
		filters = [];
		grid.isotope({ filter: filter });
	}
	
	$(document).on("click", ".grid-display", function(f) {
		f.preventDefault();
		if (viewportSize.getWidth() < 768) {
			$(this).parent().addClass("show");
		}
	});
	
	$(document).on("click", ".grid-box", function() {
		if (viewportSize.getWidth() < 768) {
			$(this).parent().removeClass("show");
		}
	});
	
	$(document).on("click", ".scroll-to", function(f) {
		f.preventDefault();
		$("body").scrollTo($("#"+$(this).data("href")).offset().top - $("#top").outerHeight(), 1000);
	});
      
	$(".review-slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		prevArrow: $(".prv-arrw"),
		nextArrow: $(".nxt-arrw"),
	});
	
	$(document).on("click", "#scroll-top", function() {
	    $(window).scrollTo(0, 1000);
	});
	
	function setHeightTimeline() {	
		$.each($(".year_storys"), function(i, val1) {
			$year_height = 0;
			$year_left_height = 0;
			$year_right_height = 0;
			$year_scale = 1;
			$year_text = "";
			
			$.each($(".time-line-item", val1), function(j, val2) {
				$mark_height = $(val2).outerHeight()+10;
				if ($(window).outerWidth() < 768) {
					$mark_height += 18;
				}
				if ($(val2).hasClass("year-mark")) {
					$year_text = $(val2).find(".time-line-date").text();
				}
				$year_height += $mark_height;
			});
			$.each($(".left_story", val1), function(j, val3) {
				$story_left_height = $(val3).outerHeight();
				$year_left_height += $story_left_height;
			});
			$.each($(".right_story", val1), function(j, val4) {
				$story_right_height = $(val4).outerHeight();
				$year_right_height += $story_right_height;
			});
			if ($(window).outerWidth() < 768) {
				$year_scale = 1.1;
				$year_height += $year_left_height + $year_right_height;
				$year_height = $year_height * $year_scale; 
			} else {
				$year_scale = 1.2;
				if ($year_left_height > $year_right_height) {
					$year_height += $year_left_height;
				} else {
					$year_height += $year_right_height;
				}
				$year_height = $year_height * $year_scale;
			}
			//console.log("Height of year: "+ $year_text +" / left: "+ $year_left_height +"px / right: "+ $year_right_height +"px / sum scaled by "+ $year_scale +": "+ $year_height.toFixed(1) +"px");
			$(val1).css("min-height", $year_height);
			$(".time-line-rel", val1).css("height", $year_height);
		});
    }
	
	
	var wow = new WOW({
		boxClass:     "wow",
		animateClass: "animated",
		offset:       0,
		mobile:       true,
		live:         true
	});
	wow.init();
	
	
	if ($("#map").length) { 	
		var center = SMap.Coords.fromWGS84(17.460077, 49.069630);
		var m = new SMap(JAK.gel("map"), center, 17);
		var l = m.addDefaultLayer(SMap.DEF_BASE).enable();
		m.addDefaultControls();		
		var layer = new SMap.Layer.Marker();
		m.addLayer(layer);
		layer.enable();		  
		var coords = SMap.Coords.fromWGS84(17.460077, 49.069630);	  
		var options = {};	 
		var marker = new SMap.Marker(coords, "myMarker", options);
		layer.addMarker(marker);
		var mouseControl = null;
		var controls = m.getControls();

		for (var i = 0; i < controls.length; i++) {

			if (controls[i] instanceof SMap.Control.Mouse) {
				mouseControl = controls[i];
			}
		}
		mouseControl.configure(SMap.MOUSE_PAN | SMap.MOUSE_ZOOM);
	}

});