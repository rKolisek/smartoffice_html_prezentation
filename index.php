<!doctype html>
<html lang="cs">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/dist/main.css" >
        
        <title>PP Soft</title>
    </head>
    <body>
    	<?php
            $page = "homepage";
            if (isset($_GET["page"]) && !empty($_GET["page"])) {
                $page = $_GET["page"];
            }
            $fix = "";
            if (in_array($page, array("homepage"))) {
                $fix = "fix";
            }
        ?>
        <div id="mobile-menu">
            <div id="inside">
            	<div class="container-menu">
            		<div class="row px-3 px-sm-3">
            			<div class="col-12 px-0 px-sm-4 logo">
                        	<a href="/" >
                        		<img src="/images/svg/ppsoft.svg" alt="ppsoft-logo"/>
                       		</a>
                		</div>
            		</div>
            	</div>
            	<div id="menu-response"></div>
            </div>
        </div>
        <div id="top" class="sps <?php echo $fix ?>" data-sps-offset="0">
           <div class="container-fluid">
           		<div class="row px-3 px-sm-0">
           			<div class="col-6 col-sm-4 col-lg-2 px-0 logo">
                    	<a href="/" >
                    		<img src="/images/svg/ppsoft.svg" alt="ppsoft-logo"/>
                     		<span>Profesionální e-shop, informační systém na míru, energetický systém EIS - PP Soft, s.r.o.</span>
                   		</a>
        			</div>
           			<div class="col-6 col-sm-3 col-md-4 col-lg-9 px-0 col-xl-7 p-0">
        				<a class="cd-nav-trigger">
    						<span class="cd-icon"></span>
        				</a>
        				<nav class="navbar navbar-expand-lg navbar-light" id="main-menu">
                          <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  E-shop na míru
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="#">Co vše najdete v našem eshopu</a>
                                  <a class="dropdown-item" href="#">Lorem</a>
                                </div>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 Vývoj na míru
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 EIS
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 Články
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                              </li>
                             <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 FAQ
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                              </li>
                              <li class="nav-item">
                                    <a class="nav-link" href="#">Kariéra <span class="badge badge-info">1</span></a>
                              </li>                         
                              <li class="nav-item">
                                    <a class="nav-link" href="#">Kontakt</a>
                              </li>
                            </ul>
                          </div>
                        </nav>
        			</div>
           			<div class="col-12 col-sm-5 col-md-4 col-lg-1 col-xl-3 px-0" id="contacts-offers-panel">
           				<div class="top-phone">
           					<a href="tel:776 714 651" class="phone-icon"><img src="images/svg/phone.svg"></a>
           					<a href="tel:776 714 651" class="phone-number">776 714 651</a>
           				</div>
           				<div class="top-mail">
           					<a href="mailto:info@pp-soft.cz" class="mail-icon"><img src="images/svg/mail.svg"></a>
           					<a href="mailto:info@pp-soft.cz" class="mail-adress">info@pp-soft.cz</a>
           				</div>
           				<div class="top-offer">
           					<a href="#" class="btn btn-outline-light btn-sm">Chci nabídku</a>
           				</div>
           			</div>
           		</div>
           </div>
        </div>
        <?php
            if (file_exists("pages/".$page.".php")) {
                include_once "pages/".$page.".php";
            }
        ?>
		<div id="footer">
    		<div class="footer-layout lazy" data-src="/images/layout/footer/footer_right_bg.png">
                <div class="container">
        			<div class="row mx-0">
        				<div class="col-12 col-lg-7 footer-box">
        					<div class="underlayout lazy" data-src="/images/layout/footer/footer_left_bg.png"></div>
        					<div class="overlayout"></div>
        					<div class="col-12">
        						<div class="footer-link logo">
        							<a class="zoom-link-hover" href="/">
                						<img class="img-fluid lazy" data-src="/images/svg/ppsoft.svg" alt="ppsoft-logo">
               						</a>
           						</div>
       						</div>
        					<div class="row mx-0">
        						<div class="col-12 col-md-6 pr-0">
        							<div class="row mx-0 footer-contact">
        								<div class="col-2 p-0 text-center">
        									<img class="img-fluid lazy" data-src="/images/layout/footer/placeholder.png" alt="placeholder-icon">
        								</div>
        								<div class="col-10 pr-0">
        									<span class="bold mb-2">PP soft s.r.o.</span>Prostřední 132 <br>Uherské Hradiště, 686 01
        								</div>
        							</div>
        						</div>
        						<div class="col-12 col-md-6 pr-0">
        							<div class="row mx-0 footer-contact align-items-center">
        								<div class="col-2 p-0 text-center">
        									<img class="img-fluid lazy" data-src="/images/layout/footer/mail.png" alt="mail-icon">
        								</div>
        								<div class="col-10 pr-0">
        									<a href="mailto:info@pp-soft.cz">info@pp-soft.cz</a>
    									</div>
        							</div>
        							<div class="row mx-0 footer-contact align-items-center">
        								<div class="col-2 p-0 text-center">
        									<img class="img-fluid lazy" data-src="/images/layout/footer/phone_call.png" alt="phone-call-icon">
        								</div>
        								<div class="col-10 pr-0">
        									<a href="tel:+ 420 776 714 651">+ 420 776 714 651</a>
    									</div>
        							</div>
        						</div>
        					</div>
        					<div class="row mx-0 align-items-center">
        						<div class="col-12 col-sm-6">
        							<div class="footer-link text-center mt-3 apek">
            							<a class="zoom-link-hover" href="/">
        									<img class="img-fluid lazy" data-src="/images/layout/footer/logo-apek.png" alt="apek-logo">
        								</a>
    								</div>
        						</div>
        						<div class="col-12 col-sm-6">
        							<div class="footer-link text-center mt-3 heureka">
            							<a class="zoom-link-hover" href="/">
        									<img class="img-fluid lazy" data-src="/images/layout/footer/heureka_badge.png" alt="heureka-badge">
        								</a>
    								</div>
        						</div>
    						</div>
        				</div>
        				<div class="col-12 col-lg-5 text-center footer-box">
        					<div class="footer-social">
        						<a class="zoom-link-hover" href="/">
        							<img class="img-fluid lazy" data-src="/images/svg/facebook-logo-outline.svg" alt="facebook-logo">
        						</a>
        						<a class="zoom-link-hover" href="/">
        							<img class="img-fluid lazy" data-src="/images/svg/instagram-social-outlined-logo.svg" alt="twitter-logo">
        						</a>
        						<a class="zoom-link-hover" href="/">
        							<img class="img-fluid lazy" data-src="/images/svg/twitter-social-outlined-logo.svg" alt="instagram-logo">
        						</a>
        					</div>
        					<h4>Chci vypracovat nabídku na míru</h4>
        					<div class="footer-text">
        						<p>
    							<span class="bold">Zdarma</span> pro Vás vypracujeme nabídku přímo pro Váš projekt. Lorem a consequat laoreet netus id Suspendisse tempus pulvinar mauris elit. 
    							Dui risus velit ut ante laoreet semper wisi congue nulla ac. Dui eget Nulla ipsum tellus mus hendrerit et pede consequat Aliquam. 
    							Pellentesque Vivamus a eget eu ut In libero a interdum scelerisque. Cursus semper et Morbi at in In eu ullamcorper Curabitur faucibus. 
    							Pretium Quisque quis ut justo volutpat tempor odio metus fames metus. Consequat.
        						</p>
        					</div>
        					<a href="/" class="btn btn-off">Chci nabídku</a>
        				</div>
    				</div>
    			</div>
            </div>
    	</div>
		<div id="bottom">
    		<div class="container">
    			<div class="col-12 text-center copyright">2018 © Created by PP Soft </div>
    		</div>
    	</div>
    	<div id="scroll-top" class="scroll-up">
    		<img src="images/svg/down-arrow-scroll.svg" alt="scroll-up-arrow" class="scroll-up-arrow">
        	<img src="images/svg/mouse.svg" alt="scroll-up-mouse" class="scroll-up-mouse">					
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://api.mapy.cz/loader.js"></script>
		<script>Loader.load()</script>
		<script src="/dist/main.js" ></script>  
    </body>
</html>